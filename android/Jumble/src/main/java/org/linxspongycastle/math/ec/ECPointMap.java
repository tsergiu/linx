package org.linxspongycastle.math.ec;

public interface ECPointMap
{
    ECPoint map(ECPoint p);
}
