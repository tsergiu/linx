package org.linxspongycastle.math.ec.endo;

import org.linxspongycastle.math.ec.ECPointMap;

public interface ECEndomorphism
{
    ECPointMap getPointMap();

    boolean hasEfficientPointMap();
}
