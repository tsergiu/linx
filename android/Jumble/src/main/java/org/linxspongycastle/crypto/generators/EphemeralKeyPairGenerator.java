package org.linxspongycastle.crypto.generators;

import org.linxspongycastle.crypto.AsymmetricCipherKeyPair;
import org.linxspongycastle.crypto.AsymmetricCipherKeyPairGenerator;
import org.linxspongycastle.crypto.EphemeralKeyPair;
import org.linxspongycastle.crypto.KeyEncoder;

public class EphemeralKeyPairGenerator
{
    private AsymmetricCipherKeyPairGenerator gen;
    private KeyEncoder keyEncoder;

    public EphemeralKeyPairGenerator(AsymmetricCipherKeyPairGenerator gen, KeyEncoder keyEncoder)
    {
        this.gen = gen;
        this.keyEncoder = keyEncoder;
    }

    public EphemeralKeyPair generate()
    {
        AsymmetricCipherKeyPair eph = gen.generateKeyPair();

        // Encode the ephemeral public key
         return new EphemeralKeyPair(eph, keyEncoder);
    }
}
