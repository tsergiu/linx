package org.linxspongycastle.crypto.ec;

import org.linxspongycastle.crypto.CipherParameters;
import org.linxspongycastle.math.ec.ECPoint;

public interface ECEncryptor
{
    void init(CipherParameters params);

    ECPair encrypt(ECPoint point);
}
