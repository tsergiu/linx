package org.linxspongycastle.crypto.ec;

import org.linxspongycastle.crypto.CipherParameters;

public interface ECPairTransform
{
    void init(CipherParameters params);

    ECPair transform(ECPair cipherText);
}
