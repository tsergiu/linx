package org.linxspongycastle.crypto.ec;

import org.linxspongycastle.crypto.CipherParameters;
import org.linxspongycastle.math.ec.ECPoint;

public interface ECDecryptor
{
    void init(CipherParameters params);

    ECPoint decrypt(ECPair cipherText);
}
