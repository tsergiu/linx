package org.linxspongycastle.crypto.tls;

public abstract class AbstractTlsAgreementCredentials
    extends AbstractTlsCredentials
    implements TlsAgreementCredentials
{
}
