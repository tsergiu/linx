package org.linxspongycastle.crypto.tls;

import org.linxspongycastle.crypto.DSA;
import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;
import org.linxspongycastle.crypto.params.ECPublicKeyParameters;
import org.linxspongycastle.crypto.signers.ECDSASigner;
import org.linxspongycastle.crypto.signers.HMacDSAKCalculator;

public class TlsECDSASigner
    extends TlsDSASigner
{
    public boolean isValidPublicKey(AsymmetricKeyParameter publicKey)
    {
        return publicKey instanceof ECPublicKeyParameters;
    }

    protected DSA createDSAImpl(short hashAlgorithm)
    {
        return new ECDSASigner(new HMacDSAKCalculator(TlsUtils.createHash(hashAlgorithm)));
    }

    protected short getSignatureAlgorithm()
    {
        return SignatureAlgorithm.ecdsa;
    }
}
