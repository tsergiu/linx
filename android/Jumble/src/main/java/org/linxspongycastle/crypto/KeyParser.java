package org.linxspongycastle.crypto;

import java.io.IOException;
import java.io.InputStream;

import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;

public interface KeyParser
{
    AsymmetricKeyParameter readKey(InputStream stream)
        throws IOException;
}
