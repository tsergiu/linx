package org.linxspongycastle.crypto.prng;

import org.linxspongycastle.crypto.prng.drbg.SP80090DRBG;

interface DRBGProvider
{
    SP80090DRBG get(EntropySource entropySource);
}
