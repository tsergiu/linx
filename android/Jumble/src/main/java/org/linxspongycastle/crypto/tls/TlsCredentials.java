package org.linxspongycastle.crypto.tls;

public interface TlsCredentials
{
    Certificate getCertificate();
}
