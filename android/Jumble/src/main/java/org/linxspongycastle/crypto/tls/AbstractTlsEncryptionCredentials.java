package org.linxspongycastle.crypto.tls;

public abstract class AbstractTlsEncryptionCredentials
    extends AbstractTlsCredentials
    implements TlsEncryptionCredentials
{
}
