package org.linxspongycastle.cert.path;

import org.linxspongycastle.cert.X509CertificateHolder;
import org.linxspongycastle.util.Memoable;

public interface CertPathValidation
    extends Memoable
{
    public void validate(CertPathValidationContext context, X509CertificateHolder certificate)
        throws CertPathValidationException;
}
