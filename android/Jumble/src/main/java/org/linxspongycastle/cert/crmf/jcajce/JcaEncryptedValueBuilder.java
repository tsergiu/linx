package org.linxspongycastle.cert.crmf.jcajce;

import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;

import org.linxspongycastle.asn1.crmf.EncryptedValue;
import org.linxspongycastle.cert.crmf.CRMFException;
import org.linxspongycastle.cert.crmf.EncryptedValueBuilder;
import org.linxspongycastle.cert.jcajce.JcaX509CertificateHolder;
import org.linxspongycastle.operator.KeyWrapper;
import org.linxspongycastle.operator.OutputEncryptor;

public class JcaEncryptedValueBuilder
    extends EncryptedValueBuilder
{
    public JcaEncryptedValueBuilder(KeyWrapper wrapper, OutputEncryptor encryptor)
    {
        super(wrapper, encryptor);
    }

    public EncryptedValue build(X509Certificate certificate)
        throws CertificateEncodingException, CRMFException
    {
        return build(new JcaX509CertificateHolder(certificate));
    }
}
