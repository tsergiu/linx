package org.linxspongycastle.cert.ocsp.jcajce;

import java.security.PublicKey;

import org.linxspongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.linxspongycastle.cert.ocsp.BasicOCSPRespBuilder;
import org.linxspongycastle.cert.ocsp.OCSPException;
import org.linxspongycastle.operator.DigestCalculator;

public class JcaBasicOCSPRespBuilder
    extends BasicOCSPRespBuilder
{
    public JcaBasicOCSPRespBuilder(PublicKey key, DigestCalculator digCalc)
        throws OCSPException
    {
        super(SubjectPublicKeyInfo.getInstance(key.getEncoded()), digCalc);
    }
}
