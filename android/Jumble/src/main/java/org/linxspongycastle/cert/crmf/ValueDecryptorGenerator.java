package org.linxspongycastle.cert.crmf;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.operator.InputDecryptor;

public interface ValueDecryptorGenerator
{
    InputDecryptor getValueDecryptor(AlgorithmIdentifier keyAlg, AlgorithmIdentifier symmAlg, byte[] encKey)
        throws CRMFException;
}
