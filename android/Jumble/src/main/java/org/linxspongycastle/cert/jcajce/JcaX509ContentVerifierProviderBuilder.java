package org.linxspongycastle.cert.jcajce;

import java.security.Provider;
import java.security.cert.CertificateException;

import org.linxspongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.linxspongycastle.cert.X509CertificateHolder;
import org.linxspongycastle.cert.X509ContentVerifierProviderBuilder;
import org.linxspongycastle.operator.ContentVerifierProvider;
import org.linxspongycastle.operator.OperatorCreationException;
import org.linxspongycastle.operator.jcajce.JcaContentVerifierProviderBuilder;

public class JcaX509ContentVerifierProviderBuilder
    implements X509ContentVerifierProviderBuilder
{
    private JcaContentVerifierProviderBuilder builder = new JcaContentVerifierProviderBuilder();

    public JcaX509ContentVerifierProviderBuilder setProvider(Provider provider)
    {
        this.builder.setProvider(provider);

        return this;
    }

    public JcaX509ContentVerifierProviderBuilder setProvider(String providerName)
    {
        this.builder.setProvider(providerName);

        return this;
    }

    public ContentVerifierProvider build(SubjectPublicKeyInfo validatingKeyInfo)
        throws OperatorCreationException
    {
        return builder.build(validatingKeyInfo);
    }

    public ContentVerifierProvider build(X509CertificateHolder validatingKeyInfo)
        throws OperatorCreationException
    {
        try
        {
            return builder.build(validatingKeyInfo);
        }
        catch (CertificateException e)
        {
            throw new OperatorCreationException("Unable to process certificate: " + e.getMessage(), e);
        }
    }
}
