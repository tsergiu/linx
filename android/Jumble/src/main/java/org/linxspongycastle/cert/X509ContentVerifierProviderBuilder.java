package org.linxspongycastle.cert;

import org.linxspongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.linxspongycastle.operator.ContentVerifierProvider;
import org.linxspongycastle.operator.OperatorCreationException;

public interface X509ContentVerifierProviderBuilder
{
    ContentVerifierProvider build(SubjectPublicKeyInfo validatingKeyInfo)
        throws OperatorCreationException;

    ContentVerifierProvider build(X509CertificateHolder validatingKeyInfo)
        throws OperatorCreationException;
}
