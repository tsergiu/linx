package org.linxspongycastle.jcajce.provider.symmetric;

import org.linxspongycastle.crypto.BlockCipher;
import org.linxspongycastle.crypto.CipherKeyGenerator;
import org.linxspongycastle.crypto.engines.SerpentEngine;
import org.linxspongycastle.crypto.engines.TwofishEngine;
import org.linxspongycastle.crypto.generators.Poly1305KeyGenerator;
import org.linxspongycastle.crypto.macs.GMac;
import org.linxspongycastle.crypto.modes.GCMBlockCipher;
import org.linxspongycastle.jcajce.provider.config.ConfigurableProvider;
import org.linxspongycastle.jcajce.provider.symmetric.util.BaseBlockCipher;
import org.linxspongycastle.jcajce.provider.symmetric.util.BaseKeyGenerator;
import org.linxspongycastle.jcajce.provider.symmetric.util.BaseMac;
import org.linxspongycastle.jcajce.provider.symmetric.util.BlockCipherProvider;
import org.linxspongycastle.jcajce.provider.symmetric.util.IvAlgorithmParameters;

public final class Serpent
{
    private Serpent()
    {
    }
    
    public static class ECB
        extends BaseBlockCipher
    {
        public ECB()
        {
            super(new BlockCipherProvider()
            {
                public BlockCipher get()
                {
                    return new SerpentEngine();
                }
            });
        }
    }

    public static class KeyGen
        extends BaseKeyGenerator
    {
        public KeyGen()
        {
            super("Serpent", 192, new CipherKeyGenerator());
        }
    }

    public static class SerpentGMAC
        extends BaseMac
    {
        public SerpentGMAC()
        {
            super(new GMac(new GCMBlockCipher(new SerpentEngine())));
        }
    }

    public static class Poly1305
        extends BaseMac
    {
        public Poly1305()
        {
            super(new org.linxspongycastle.crypto.macs.Poly1305(new TwofishEngine()));
        }
    }

    public static class Poly1305KeyGen
        extends BaseKeyGenerator
    {
        public Poly1305KeyGen()
        {
            super("Poly1305-Serpent", 256, new Poly1305KeyGenerator());
        }
    }

    public static class AlgParams
        extends IvAlgorithmParameters
    {
        protected String engineToString()
        {
            return "Serpent IV";
        }
    }

    public static class Mappings
        extends SymmetricAlgorithmProvider
    {
        private static final String PREFIX = Serpent.class.getName();

        public Mappings()
        {
        }

        public void configure(ConfigurableProvider provider)
        {

            provider.addAlgorithm("Cipher.Serpent", PREFIX + "$ECB");
            provider.addAlgorithm("KeyGenerator.Serpent", PREFIX + "$KeyGen");
            provider.addAlgorithm("AlgorithmParameters.Serpent", PREFIX + "$AlgParams");

            addGMacAlgorithm(provider, "SERPENT", PREFIX + "$SerpentGMAC", PREFIX + "$KeyGen");
            addPoly1305Algorithm(provider, "SERPENT", PREFIX + "$Poly1305", PREFIX + "$Poly1305KeyGen");
        }
    }
}
