package org.linxspongycastle.jcajce.provider.util;

import org.linxspongycastle.jcajce.provider.config.ConfigurableProvider;

public abstract class AlgorithmProvider
{
    public abstract void configure(ConfigurableProvider provider);
}
