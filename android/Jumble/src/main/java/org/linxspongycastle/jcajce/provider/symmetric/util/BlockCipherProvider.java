package org.linxspongycastle.jcajce.provider.symmetric.util;

import org.linxspongycastle.crypto.BlockCipher;

public interface BlockCipherProvider
{
    BlockCipher get();
}
