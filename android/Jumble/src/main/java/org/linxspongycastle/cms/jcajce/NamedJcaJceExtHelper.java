package org.linxspongycastle.cms.jcajce;

import java.security.PrivateKey;

import javax.crypto.SecretKey;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.jcajce.util.NamedJcaJceHelper;
import org.linxspongycastle.operator.SymmetricKeyUnwrapper;
import org.linxspongycastle.operator.jcajce.JceAsymmetricKeyUnwrapper;
import org.linxspongycastle.operator.jcajce.JceSymmetricKeyUnwrapper;

class NamedJcaJceExtHelper
    extends NamedJcaJceHelper
    implements JcaJceExtHelper
{
    public NamedJcaJceExtHelper(String providerName)
    {
        super(providerName);
    }

    public JceAsymmetricKeyUnwrapper createAsymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, PrivateKey keyEncryptionKey)
    {
        return new JceAsymmetricKeyUnwrapper(keyEncryptionAlgorithm, keyEncryptionKey).setProvider(providerName);
    }

    public SymmetricKeyUnwrapper createSymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, SecretKey keyEncryptionKey)
    {
        return new JceSymmetricKeyUnwrapper(keyEncryptionAlgorithm, keyEncryptionKey).setProvider(providerName);
    }
}