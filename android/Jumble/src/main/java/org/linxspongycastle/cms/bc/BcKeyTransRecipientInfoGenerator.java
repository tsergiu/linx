package org.linxspongycastle.cms.bc;

import org.linxspongycastle.asn1.cms.IssuerAndSerialNumber;
import org.linxspongycastle.cert.X509CertificateHolder;
import org.linxspongycastle.cms.KeyTransRecipientInfoGenerator;
import org.linxspongycastle.operator.bc.BcAsymmetricKeyWrapper;

public abstract class BcKeyTransRecipientInfoGenerator
    extends KeyTransRecipientInfoGenerator
{
    public BcKeyTransRecipientInfoGenerator(X509CertificateHolder recipientCert, BcAsymmetricKeyWrapper wrapper)
    {
        super(new IssuerAndSerialNumber(recipientCert.toASN1Structure()), wrapper);
    }

    public BcKeyTransRecipientInfoGenerator(byte[] subjectKeyIdentifier, BcAsymmetricKeyWrapper wrapper)
    {
        super(subjectKeyIdentifier, wrapper);
    }
}