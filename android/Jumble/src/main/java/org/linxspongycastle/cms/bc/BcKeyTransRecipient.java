package org.linxspongycastle.cms.bc;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.cms.CMSException;
import org.linxspongycastle.cms.KeyTransRecipient;
import org.linxspongycastle.crypto.CipherParameters;
import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;
import org.linxspongycastle.operator.AsymmetricKeyUnwrapper;
import org.linxspongycastle.operator.OperatorException;
import org.linxspongycastle.operator.bc.BcRSAAsymmetricKeyUnwrapper;

public abstract class BcKeyTransRecipient
    implements KeyTransRecipient
{
    private AsymmetricKeyParameter recipientKey;

    public BcKeyTransRecipient(AsymmetricKeyParameter recipientKey)
    {
        this.recipientKey = recipientKey;
    }

    protected CipherParameters extractSecretKey(AlgorithmIdentifier keyEncryptionAlgorithm, AlgorithmIdentifier encryptedKeyAlgorithm, byte[] encryptedEncryptionKey)
        throws CMSException
    {
        AsymmetricKeyUnwrapper unwrapper = new BcRSAAsymmetricKeyUnwrapper(keyEncryptionAlgorithm, recipientKey);

        try
        {
            return CMSUtils.getBcKey(unwrapper.generateUnwrappedKey(encryptedKeyAlgorithm, encryptedEncryptionKey));
        }
        catch (OperatorException e)
        {
            throw new CMSException("exception unwrapping key: " + e.getMessage(), e);
        }
    }
}
