package org.linxspongycastle.cms.jcajce;

import java.security.PrivateKey;

import javax.crypto.SecretKey;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.jcajce.util.DefaultJcaJceHelper;
import org.linxspongycastle.operator.SymmetricKeyUnwrapper;
import org.linxspongycastle.operator.jcajce.JceAsymmetricKeyUnwrapper;
import org.linxspongycastle.operator.jcajce.JceSymmetricKeyUnwrapper;

class DefaultJcaJceExtHelper
    extends DefaultJcaJceHelper
    implements JcaJceExtHelper
{
    public JceAsymmetricKeyUnwrapper createAsymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, PrivateKey keyEncryptionKey)
    {
        return new JceAsymmetricKeyUnwrapper(keyEncryptionAlgorithm, keyEncryptionKey);
    }

    public SymmetricKeyUnwrapper createSymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, SecretKey keyEncryptionKey)
    {
        return new JceSymmetricKeyUnwrapper(keyEncryptionAlgorithm, keyEncryptionKey);
    }
}
