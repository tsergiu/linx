package org.linxspongycastle.cms;

public class CMSSignerDigestMismatchException
    extends CMSException
{
    public CMSSignerDigestMismatchException(
        String msg)
    {
        super(msg);
    }
}
