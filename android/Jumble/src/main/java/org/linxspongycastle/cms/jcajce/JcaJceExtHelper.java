package org.linxspongycastle.cms.jcajce;

import java.security.PrivateKey;

import javax.crypto.SecretKey;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.jcajce.util.JcaJceHelper;
import org.linxspongycastle.operator.SymmetricKeyUnwrapper;
import org.linxspongycastle.operator.jcajce.JceAsymmetricKeyUnwrapper;

public interface JcaJceExtHelper
    extends JcaJceHelper
{
    JceAsymmetricKeyUnwrapper createAsymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, PrivateKey keyEncryptionKey);

    SymmetricKeyUnwrapper createSymmetricUnwrapper(AlgorithmIdentifier keyEncryptionAlgorithm, SecretKey keyEncryptionKey);
}
