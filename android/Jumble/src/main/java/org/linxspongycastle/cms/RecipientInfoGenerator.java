package org.linxspongycastle.cms;

import org.linxspongycastle.asn1.cms.RecipientInfo;
import org.linxspongycastle.operator.GenericKey;

public interface RecipientInfoGenerator
{
    RecipientInfo generate(GenericKey contentEncryptionKey)
        throws CMSException;
}
