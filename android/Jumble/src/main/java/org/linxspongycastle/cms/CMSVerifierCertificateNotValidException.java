package org.linxspongycastle.cms;

public class CMSVerifierCertificateNotValidException
    extends CMSException
{
    public CMSVerifierCertificateNotValidException(
        String msg)
    {
        super(msg);
    }
}
