package org.linxspongycastle.cms;

import org.linxspongycastle.asn1.ASN1Set;

interface AuthAttributesProvider
{
    ASN1Set getAuthAttributes();
}
