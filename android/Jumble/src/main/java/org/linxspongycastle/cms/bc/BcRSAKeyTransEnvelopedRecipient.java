package org.linxspongycastle.cms.bc;

import java.io.InputStream;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.cms.CMSException;
import org.linxspongycastle.cms.RecipientOperator;
import org.linxspongycastle.crypto.BufferedBlockCipher;
import org.linxspongycastle.crypto.CipherParameters;
import org.linxspongycastle.crypto.StreamCipher;
import org.linxspongycastle.crypto.io.CipherInputStream;
import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;
import org.linxspongycastle.operator.InputDecryptor;

public class BcRSAKeyTransEnvelopedRecipient
    extends BcKeyTransRecipient
{
    public BcRSAKeyTransEnvelopedRecipient(AsymmetricKeyParameter key)
    {
        super(key);
    }

    public RecipientOperator getRecipientOperator(AlgorithmIdentifier keyEncryptionAlgorithm, final AlgorithmIdentifier contentEncryptionAlgorithm, byte[] encryptedContentEncryptionKey)
        throws CMSException
    {
        CipherParameters secretKey = extractSecretKey(keyEncryptionAlgorithm, contentEncryptionAlgorithm, encryptedContentEncryptionKey);

        final Object dataCipher = EnvelopedDataHelper.createContentCipher(false, secretKey, contentEncryptionAlgorithm);

        return new RecipientOperator(new InputDecryptor()
        {
            public AlgorithmIdentifier getAlgorithmIdentifier()
            {
                return contentEncryptionAlgorithm;
            }

            public InputStream getInputStream(InputStream dataIn)
            {
                if (dataCipher instanceof BufferedBlockCipher)
                {
                    return new CipherInputStream(dataIn, (BufferedBlockCipher)dataCipher);
                }
                else
                {
                    return new CipherInputStream(dataIn, (StreamCipher)dataCipher);
                }
            }
        });
    }
}
