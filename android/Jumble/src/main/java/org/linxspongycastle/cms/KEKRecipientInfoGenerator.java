package org.linxspongycastle.cms;

import org.linxspongycastle.asn1.ASN1OctetString;
import org.linxspongycastle.asn1.DEROctetString;
import org.linxspongycastle.asn1.cms.KEKIdentifier;
import org.linxspongycastle.asn1.cms.KEKRecipientInfo;
import org.linxspongycastle.asn1.cms.RecipientInfo;
import org.linxspongycastle.operator.GenericKey;
import org.linxspongycastle.operator.OperatorException;
import org.linxspongycastle.operator.SymmetricKeyWrapper;

public abstract class KEKRecipientInfoGenerator
    implements RecipientInfoGenerator
{
    private final KEKIdentifier kekIdentifier;

    protected final SymmetricKeyWrapper wrapper;

    protected KEKRecipientInfoGenerator(KEKIdentifier kekIdentifier, SymmetricKeyWrapper wrapper)
    {
        this.kekIdentifier = kekIdentifier;
        this.wrapper = wrapper;
    }

    public final RecipientInfo generate(GenericKey contentEncryptionKey)
        throws CMSException
    {
        try
        {
            ASN1OctetString encryptedKey = new DEROctetString(wrapper.generateWrappedKey(contentEncryptionKey));

            return new RecipientInfo(new KEKRecipientInfo(kekIdentifier, wrapper.getAlgorithmIdentifier(), encryptedKey));
        }
        catch (OperatorException e)
        {
            throw new CMSException("exception wrapping content key: " + e.getMessage(), e);
        }
    }
}