package org.linxspongycastle.pkcs;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.operator.MacCalculator;
import org.linxspongycastle.operator.OperatorCreationException;

public interface PKCS12MacCalculatorBuilder
{
    MacCalculator build(char[] password)
        throws OperatorCreationException;

    AlgorithmIdentifier getDigestAlgorithmIdentifier();
}
