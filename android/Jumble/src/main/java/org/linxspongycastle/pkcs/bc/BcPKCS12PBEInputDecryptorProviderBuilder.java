package org.linxspongycastle.pkcs.bc;

import java.io.InputStream;

import org.linxspongycastle.asn1.pkcs.PKCS12PBEParams;
import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.crypto.CipherParameters;
import org.linxspongycastle.crypto.ExtendedDigest;
import org.linxspongycastle.crypto.digests.SHA1Digest;
import org.linxspongycastle.crypto.generators.PKCS12ParametersGenerator;
import org.linxspongycastle.crypto.io.CipherInputStream;
import org.linxspongycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.linxspongycastle.operator.GenericKey;
import org.linxspongycastle.operator.InputDecryptor;
import org.linxspongycastle.operator.InputDecryptorProvider;

public class BcPKCS12PBEInputDecryptorProviderBuilder
{
    private ExtendedDigest digest;

    public BcPKCS12PBEInputDecryptorProviderBuilder()
    {
         this(new SHA1Digest());
    }

    public BcPKCS12PBEInputDecryptorProviderBuilder(ExtendedDigest digest)
    {
         this.digest = digest;
    }

    public InputDecryptorProvider build(final char[] password)
    {
        return new InputDecryptorProvider()
        {
            public InputDecryptor get(final AlgorithmIdentifier algorithmIdentifier)
            {
                final PaddedBufferedBlockCipher engine = PKCS12PBEUtils.getEngine(algorithmIdentifier.getAlgorithm());

                PKCS12PBEParams           pbeParams = PKCS12PBEParams.getInstance(algorithmIdentifier.getParameters());

                CipherParameters params = PKCS12PBEUtils.createCipherParameters(algorithmIdentifier.getAlgorithm(), digest, engine.getBlockSize(), pbeParams, password);

                engine.init(false, params);

                return new InputDecryptor()
                {
                    public AlgorithmIdentifier getAlgorithmIdentifier()
                    {
                        return algorithmIdentifier;
                    }

                    public InputStream getInputStream(InputStream input)
                    {
                        return new CipherInputStream(input, engine);
                    }

                    public GenericKey getKey()
                    {
                        return new GenericKey(PKCS12ParametersGenerator.PKCS12PasswordToBytes(password));
                    }
                };
            }
        };

    }
}
