package org.linxspongycastle.pqc.crypto.gmss;

import org.linxspongycastle.crypto.Digest;

public interface GMSSDigestProvider
{
    Digest get();
}
