package org.linxspongycastle.jce.interfaces;

import org.linxspongycastle.jce.spec.ElGamalParameterSpec;

public interface ElGamalKey
{
    public ElGamalParameterSpec getParameters();
}
