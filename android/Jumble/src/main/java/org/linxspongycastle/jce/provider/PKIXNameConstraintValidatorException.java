package org.linxspongycastle.jce.provider;

public class PKIXNameConstraintValidatorException
    extends Exception
{
    public PKIXNameConstraintValidatorException(String msg)
    {
        super(msg);
    }
}
