package org.linxspongycastle.asn1;

public interface ASN1String
{
    public String getString();
}
