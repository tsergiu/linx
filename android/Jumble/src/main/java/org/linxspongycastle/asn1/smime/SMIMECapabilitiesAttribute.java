package org.linxspongycastle.asn1.smime;

import org.linxspongycastle.asn1.DERSequence;
import org.linxspongycastle.asn1.DERSet;
import org.linxspongycastle.asn1.cms.Attribute;

public class SMIMECapabilitiesAttribute
    extends Attribute
{
    public SMIMECapabilitiesAttribute(
        SMIMECapabilityVector capabilities)
    {
        super(SMIMEAttributes.smimeCapabilities,
                new DERSet(new DERSequence(capabilities.toASN1EncodableVector())));
    }
}
