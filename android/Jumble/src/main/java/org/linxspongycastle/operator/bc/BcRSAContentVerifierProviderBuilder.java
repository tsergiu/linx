package org.linxspongycastle.operator.bc;

import java.io.IOException;

import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.asn1.x509.SubjectPublicKeyInfo;
import org.linxspongycastle.crypto.Digest;
import org.linxspongycastle.crypto.Signer;
import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;
import org.linxspongycastle.crypto.signers.RSADigestSigner;
import org.linxspongycastle.crypto.util.PublicKeyFactory;
import org.linxspongycastle.operator.DigestAlgorithmIdentifierFinder;
import org.linxspongycastle.operator.OperatorCreationException;

public class BcRSAContentVerifierProviderBuilder
    extends BcContentVerifierProviderBuilder
{
    private DigestAlgorithmIdentifierFinder digestAlgorithmFinder;

    public BcRSAContentVerifierProviderBuilder(DigestAlgorithmIdentifierFinder digestAlgorithmFinder)
    {
        this.digestAlgorithmFinder = digestAlgorithmFinder;
    }

    protected Signer createSigner(AlgorithmIdentifier sigAlgId)
        throws OperatorCreationException
    {
        AlgorithmIdentifier digAlg = digestAlgorithmFinder.find(sigAlgId);
        Digest dig = digestProvider.get(digAlg);

        return new RSADigestSigner(dig);
    }

    protected AsymmetricKeyParameter extractKeyParameters(SubjectPublicKeyInfo publicKeyInfo)
        throws IOException
    {
        return PublicKeyFactory.createKey(publicKeyInfo);
    }
}