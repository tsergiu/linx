package org.linxspongycastle.operator.bc;

import org.linxspongycastle.asn1.ASN1ObjectIdentifier;
import org.linxspongycastle.asn1.x509.AlgorithmIdentifier;
import org.linxspongycastle.crypto.AsymmetricBlockCipher;
import org.linxspongycastle.crypto.encodings.PKCS1Encoding;
import org.linxspongycastle.crypto.engines.RSAEngine;
import org.linxspongycastle.crypto.params.AsymmetricKeyParameter;

public class BcRSAAsymmetricKeyUnwrapper
    extends BcAsymmetricKeyUnwrapper
{
    public BcRSAAsymmetricKeyUnwrapper(AlgorithmIdentifier encAlgId, AsymmetricKeyParameter privateKey)
    {
        super(encAlgId, privateKey);
    }

    protected AsymmetricBlockCipher createAsymmetricUnwrapper(ASN1ObjectIdentifier algorithm)
    {
        return new PKCS1Encoding(new RSAEngine());
    }
}
