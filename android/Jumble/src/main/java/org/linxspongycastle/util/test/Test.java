package org.linxspongycastle.util.test;

public interface Test
{
    String getName();

    TestResult perform();
}
