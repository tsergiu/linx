package org.linxspongycastle.openssl.jcajce;

import java.security.Provider;

import org.linxspongycastle.jcajce.util.DefaultJcaJceHelper;
import org.linxspongycastle.jcajce.util.JcaJceHelper;
import org.linxspongycastle.jcajce.util.NamedJcaJceHelper;
import org.linxspongycastle.jcajce.util.ProviderJcaJceHelper;
import org.linxspongycastle.openssl.PEMDecryptor;
import org.linxspongycastle.openssl.PEMDecryptorProvider;
import org.linxspongycastle.openssl.PEMException;
import org.linxspongycastle.openssl.PasswordException;

public class JcePEMDecryptorProviderBuilder
{
    private JcaJceHelper helper = new DefaultJcaJceHelper();

    public JcePEMDecryptorProviderBuilder setProvider(Provider provider)
    {
        this.helper = new ProviderJcaJceHelper(provider);

        return this;
    }

    public JcePEMDecryptorProviderBuilder setProvider(String providerName)
    {
        this.helper = new NamedJcaJceHelper(providerName);

        return this;
    }

    public PEMDecryptorProvider build(final char[] password)
    {
        return new PEMDecryptorProvider()
        {
            public PEMDecryptor get(final String dekAlgName)
            {
                return new PEMDecryptor()
                {
                    public byte[] decrypt(byte[] keyBytes, byte[] iv)
                        throws PEMException
                    {
                        if (password == null)
                        {
                            throw new PasswordException("Password is null, but a password is required");
                        }

                        return PEMUtilities.crypt(false, helper, keyBytes, password, dekAlgName, iv);
                    }
                };
            }
        };
    }
}
