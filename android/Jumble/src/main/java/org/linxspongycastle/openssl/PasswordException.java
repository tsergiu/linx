package org.linxspongycastle.openssl;

public class PasswordException
    extends PEMException
{
    public PasswordException(String msg)
    {
        super(msg);
    }
}
