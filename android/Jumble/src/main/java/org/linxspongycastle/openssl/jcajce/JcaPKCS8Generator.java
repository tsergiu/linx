package org.linxspongycastle.openssl.jcajce;

import java.security.PrivateKey;

import org.linxspongycastle.asn1.pkcs.PrivateKeyInfo;
import org.linxspongycastle.openssl.PKCS8Generator;
import org.linxspongycastle.operator.OutputEncryptor;
import org.linxspongycastle.util.io.pem.PemGenerationException;

public class JcaPKCS8Generator
    extends PKCS8Generator
{
    public JcaPKCS8Generator(PrivateKey key, OutputEncryptor encryptor)
         throws PemGenerationException
    {
         super(PrivateKeyInfo.getInstance(key.getEncoded()), encryptor);
    }
}
