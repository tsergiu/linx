/*
 * Copyright (C) 2016 Andrew Comminos <andrew@comminos.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.morlunk.jumble.model;

import com.morlunk.jumble.protobuf.Mumble;

import java.util.ArrayList;

/**
 * Created by andrew on 28/04/16.
 */
public class WhisperTargetUsers implements WhisperTarget {

    //https://github.com/Johni0702/mumble-streams/blob/master/lib/Mumble.proto

    /**
     * // Sent by the client when it wants to register or clear whisper targets.
     //
     // Note: The first available target ID is 1 as 0 is reserved for normal
     // talking. Maximum target ID is 30.
     message VoiceTarget {
     message Target {
     // Users that are included as targets.
     repeated uint32 session = 1;
     // Channel that is included as a target.
     optional uint32 channel_id = 2;
     // ACL group that is included as a target.
     optional string group = 3;
     // True if the voice should follow links from the specified channel.
     optional bool links = 4 [default = false];
     // True if the voice should also be sent to children of the specific
     // channel.
     optional bool children = 5 [default = false];
     }
     // Voice target ID.
     optional uint32 id = 1;
     // The receivers that this voice target includes.
     repeated Target targets = 2;
     }
     */

    private ArrayList<Integer> targetSessionIds;
    public WhisperTargetUsers(ArrayList<Integer> targetSessionIds)
    {
        this.targetSessionIds = targetSessionIds;
    }

    @Override
    public Mumble.VoiceTarget.Target createTarget() {
        Mumble.VoiceTarget.Target.Builder vtb = Mumble.VoiceTarget.Target.newBuilder();
        //vtb.setLinks(false);
        //vtb.setChildren(false);
        vtb.addAllSession(targetSessionIds);
        //vtb.setChannelId(mChannel.getId());
        return vtb.build();
    }

    @Override
    public String getName() {

        return "WhisperTargetUsers";
    }
}
