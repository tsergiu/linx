package com.safemobile.linx.Enums;


public enum RegistrationStatus {
    REGISTRATION_SUCCESSFUL(0), REGISTRATION_UNSUCCESSFUL(1), DEREGISTRATION_SUCCESSFUL(2),
    DEREGISTRATION_UNSUCCESSFUL(3), UNKNOWN(99);
    private int value;

    RegistrationStatus(int value) {
        this.value = value;
    }


    public static RegistrationStatus fromInteger(int value) {
        switch (value) {
            case 0:
                return REGISTRATION_SUCCESSFUL;
            case 1:
                return REGISTRATION_UNSUCCESSFUL;
            case 2:
                return DEREGISTRATION_SUCCESSFUL;
            case 3:
                return DEREGISTRATION_UNSUCCESSFUL;
            default:
                return UNKNOWN;
        }
    }

    public static int getValue(RegistrationStatus status) {
        return status.value;
    }

}

