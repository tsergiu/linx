package com.safemobile.linx.Modules;


import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.linx.R;
import com.morlunk.jumble.JumbleService;
import com.morlunk.jumble.exception.AudioException;
import com.morlunk.jumble.model.IChannel;
import com.morlunk.jumble.model.IUser;
import com.morlunk.jumble.model.Server;
import com.morlunk.jumble.net.CryptState;
import com.morlunk.jumble.net.JumbleConnection;
import com.morlunk.jumble.util.JumbleException;
import com.morlunk.jumble.util.JumbleObserver;
import com.morlunk2.mumbleclient.Settings;
import com.morlunk2.mumbleclient.app.ServerConnectTask;
import com.morlunk2.mumbleclient.db.DatabaseCertificate;
import com.morlunk2.mumbleclient.db.DatabaseProvider;
import com.morlunk2.mumbleclient.db.PlumbleDatabase;
import com.morlunk2.mumbleclient.db.PlumbleSQLiteDatabase;
import com.morlunk2.mumbleclient.preference.PlumbleCertificateGenerateTask;
import com.morlunk2.mumbleclient.service.IPlumbleService;
import com.morlunk2.mumbleclient.service.PlumbleService;
import com.morlunk2.mumbleclient.util.JumbleServiceFragment;
import com.morlunk2.mumbleclient.util.JumbleServiceProvider;
import com.morlunk2.mumbleclient.util.PlumbleTrustStore;
import com.safemobile.linx.Enums.RegistrationStatus;

import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.POWER_SERVICE;

public class MumbleModule implements
        DatabaseProvider,
        SharedPreferences.OnSharedPreferenceChangeListener,
        JumbleServiceProvider {

    public interface StatsListener {
        void statusUpdate(String status);
    }

    public static StatsListener statsListener;

    private String LOG_TAG = "MumbleModule";
    private String username;
    private String password;

    public Context context = null;
    private PlumbleDatabase mDatabase = null;
    private Settings mSettings = null;
    private PlumbleService mService = null;

    private Timer debugTimer = null;

    private RegistrationStatus regStatus = RegistrationStatus.UNKNOWN;
    private void setRegStatus(RegistrationStatus status)
    {
        this.regStatus = status;

        if (connectionListener != null)
            connectionListener.onRegistrationStatusChanged(status);
    }

    /**
     * List of fragments to be notified about service state changes.
     */
    //private List<JumbleServiceFragment> mServiceFragments = new ArrayList<JumbleServiceFragment>();
    private String prevDisconnectedReason = "";

    public static final int MUMBLE_WAKE_LOCK = 1;
    /** Proximity lock for handset mode. */
    private PowerManager.WakeLock mMumbleLock;

    private CryptState prevCryptState;

    public interface MumbleListener
    {
        void onConnecting();

        void onConnected();

        void onDisconnected();

        void onConnectionDropped(String reason);

        void onWrongCertificate();

        void onCertificateGenerated(long certId);

        void onRegistrationStatusChanged(RegistrationStatus status);

        void onNoServerConfigurationProvided();

        void onAssetConnected(long assetId);

        void onAssetDisconnected(long assetId);
    }

    private MumbleListener connectionListener;

    public void setConnectionListener(MumbleListener listener) {
        this.connectionListener = listener;
    }

    public long serviceConnectedTimeMs = new Date().getTime() - 15000;
    private ServiceConnection mMumbleConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.e(LOG_TAG, "onServiceConnected");
            serviceConnectedTimeMs = new Date().getTime();
            mService = ((PlumbleService.PlumbleBinder) service).getService();
            mService.setSuppressNotifications(true);
            mService.registerObserver(mObserver);
            mService.clearChatNotifications(); // Clear chat notifications on resume.

//            for (JumbleServiceFragment fragment : mServiceFragments)
//                fragment.setServiceBound(true);

            /*eionut:
            // Re-show server list if we're showing a fragment that depends on the service.
            if(getSupportFragmentManager().findFragmentById(R.id.content_frame) instanceof JumbleServiceFragment &&
                    !backgroundService.mumbleModule.getService().isConnected()) {
                loadDrawerFragment(DrawerAdapter.ITEM_FAVOURITES);
            }
            */
            //eionut: updateConnectionState(getService());

            if (debugTimer == null) {
                debugTimer = new Timer();
                debugTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            JumbleConnection jc = ((PlumbleService) getService()).mConnection;
                            CryptState cs = getPackageState();
                            if (cs != null && mService != null) {
                                String status = "STATS: TCP Latency = " + cs.mTCPLatency
                                        + " UDP Latency = " + cs.mUDPLatency
                                        + " MAX Bandwidth = " + mService.getMaxBandwidth()
                                        + " CRT Bandwidth = " + mService.getCurrentBandwidth()
                                        + " good = " + cs.mUiGood + " late = " + cs.mUiLate
                                        + " lost = " + cs.mUiLost + " resync = " + cs.mUiResync
                                        + " remoteGood = " + cs.mUiRemoteGood + " remoteLate = "
                                        + cs.mUiRemoteLate + " remoteLost = " + cs.mUiRemoteLost
                                        + " remoteResync = " + cs.mUiRemoteResync;
                                //statsTextView.setText("STATS:\tTCP Latency = " + jc.getTCPLatency()
                                // + ",\tUDP Latency = " + jc.getUDPLatency() + "\ngood = "
                                // + cs.mUiGood + ",\tlate = " + cs.mUiLate + "\nlost = "
                                // + cs.mUiLost + ",\tresync = " + cs.mUiResync
                                // + "\nremoteGood = " + cs.mUiRemoteGood + ",\tremoteLate = "
                                // + cs.mUiRemoteLate + "\nremoteLost = " + cs.mUiRemoteLost
                                // + ",\tremoteResync = " + cs.mUiRemoteResync);

                                if (statsListener != null) {
                                    statsListener.statusUpdate(status);
                                    //Log.e(LOG_TAG, status);
                                }

                                if(prevCryptState != null && isInCall)
                                {
                                    // get latency of udp or tcp based on settings
                                    long latency = getForceTCP() ? cs.mTCPLatency : cs.mUDPLatency;

                                    // number of good packets in last second
                                    int goodPackets = (isIncommingCall
                                            ? cs.mUiGood - prevCryptState.mUiGood
                                            : cs.mUiRemoteGood - prevCryptState.mUiRemoteGood);

                                    // number of lsot packets in last second
                                    int lostPackets = (isIncommingCall
                                            ? cs.mUiLost - prevCryptState.mUiLost
                                            : cs.mUiRemoteLost - prevCryptState.mUiRemoteLost);

                                    //QualityOfService qoS = QualityMetricsModule.getQualityOfServiceFromLatency(latency / 1000);
                                    //QualityOfService qoSPackets = QualityMetricsModule.getQualityOfServiceFromPackets(goodPackets, lostPackets);
                                    //NotificationModule.broadcast(context, Intents.QUALITY_OF_SERVICE_CHANGED, qoS.name());
                                }

                                prevCryptState = cs;
                            }
                        }
                        catch (Exception ex)
                        {
                            //Log.e(LOG_TAG, "debugTimer Exception: " + ex.toString());
                        }

                    }
                }, 0, 1000);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.e(LOG_TAG, "onServiceDisconnected");
            serviceConnectedTimeMs = new Date().getTime() - 15000;
            if (mService != null) {
                try {
//                    for (JumbleServiceFragment fragment : mServiceFragments) {
//                        fragment.setServiceBound(false);
//                    }
                    mService.unregisterObserver(mObserver);
                    mService.setSuppressNotifications(false);
                } catch (Exception ex) {

                }

            }
            mService = null;
        }
    };


    private CryptState getPackageState()
    {
        CryptState cs = null;

        try {
            JumbleConnection jc = ((PlumbleService) getService()).mConnection;

            if (jc != null) {
                cs = jc.mCryptState;
                cs.mTCPLatency = jc.getTCPLatency();
                cs.mUDPLatency = jc.getUDPLatency();
            }
        } catch (Exception ex) {
            if(!ex.toString().contains("NotConnectedException"))
                Log.e(LOG_TAG, "GetPackageState Exception: " + ex.toString());
        }

        return cs;
    }

    private JumbleObserver mObserver = new JumbleObserver() {
        @Override
        public void onConnected() {
            Log.e(LOG_TAG, "onConnected" + " [prev disconnected reason : "
                    + prevDisconnectedReason + "]");

            //eionut: loadDrawerFragment(DrawerAdapter.ITEM_SERVER);
            //eionut: updateConnectionState(getService());

            if (prevDisconnectedReason.equals("unknown"))
                return;

            prevDisconnectedReason = "";

            updateMicAndSpeakerForCall(false, false);

            setRegStatus(RegistrationStatus.REGISTRATION_SUCCESSFUL);

            if (connectionListener != null)
                connectionListener.onConnected();

            IChannel channel = findChannelWithName("g-1084", mService.getRootChannel());
            if(channel != null) {
                //mService.moveUserToChannel(mService.getSessionId(), channel.getId());

                mService.joinChannel(channel.getId());
            }
            updateMicAndSpeakerForCall(true, true);
        }

        @Override
        public void onConnecting() {
            Log.e(LOG_TAG, "onConnecting");

            if (connectionListener != null)
                connectionListener.onConnecting();
        }

        @Override
        public void onDisconnected(JumbleException e) {
            Log.e(LOG_TAG, "onDisconnected " + (e == null ? "NULL" : e.toString()));

            if(e!= null && e.toString().contains("Wrong certificate or password for existing user")) {

                removeCertificate();
                if (connectionListener != null)
                    connectionListener.onWrongCertificate();
            }

            if (e == null) {
                prevDisconnectedReason = "unknown";
                if (connectionListener != null)
                    connectionListener.onConnectionDropped("unknown");
                return;
            }

            /*eionut
            // Re-show server list if we're showing a fragment that depends on the service.
            if(getSupportFragmentManager().findFragmentById(R.id.content_frame) instanceof JumbleServiceFragment) {
                loadDrawerFragment(DrawerAdapter.ITEM_FAVOURITES);
            }*/

            // do not handle twice the same error
            if (e.toString().equals(prevDisconnectedReason))
                return;

            prevDisconnectedReason = e.toString();

            setRegStatus(RegistrationStatus.REGISTRATION_UNSUCCESSFUL);

            //eionut: updateConnectionState(getService());
            /*if (e.toString().contains("Wrong certificate or password for existing user")) {

                if (connectionListener != null)
                    connectionListener.onWrongCertificate();
            } else*/ if (e.toString().contains("An error occurred when communicating with the host")) {
                if (connectionListener != null)
                    connectionListener.onConnectionDropped(e.toString());
            } else {
                if (connectionListener != null)
                    connectionListener.onConnectionDropped(e.toString());
            }
        }

        @Override
        public void onTLSHandshakeFailed(X509Certificate[] chain) {
            Log.e(LOG_TAG, "onTLSHandshakeFailed");
            final Server server = getService().getTargetServer();

            if (chain.length == 0)
                return;

            final X509Certificate x509 = chain[0];

            // Try to add to trust store
            try {
                String alias = server.getHost();
                KeyStore trustStore = PlumbleTrustStore.getTrustStore(context);
                trustStore.setCertificateEntry(alias, x509);
                PlumbleTrustStore.saveTrustStore(context, trustStore);
                Toast.makeText(context, R.string.trust_added, Toast.LENGTH_LONG).show();

                Thread.sleep(1000);
                stopMumbleService();
                startMumbleService(server);
            } catch (Exception e) {
                e.printStackTrace();

                Log.e(LOG_TAG, "onTLSHandshakeFailed");

                Toast.makeText(context, R.string.trust_add_failed, Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onPermissionDenied(String reason) {
            Log.e(LOG_TAG, "onPermissionDenied");
            Toast.makeText(context, "onPermissionDenied", Toast.LENGTH_LONG).show();
        }


        @Override
        public void onUserStateUpdated(IUser user) {
            Log.e(LOG_TAG,"onUserStateUpdated " + (user != null ?
                    (user.getUserId() + "." + user.getName() + " | "
                            + (user.isSelfDeafened() ? "deafend   " : "listening" ) + "  |  "
                            + (user.isSelfMuted() ? "muted   " : "speaking")) : ""));
/*
            Log.e("Mumble: onUserStateUpdated: user = "
                    + user.getName() + " | Session: " + user.getSession() + " | Channel: " + user.getChannel()
                    + " | Muted: " + user.isMuted() + " | Deafend: " + user.isDeafened()
                    + " | SelfMuted: " + user.isSelfMuted() + " | SelfDeafend: " + user.isSelfDeafened()
                    + " | LocalMuted: " + user.isLocalMuted()
                    + " \n "  + user.toString() );*/
        }

        @Override
        public void onUserTalkStateUpdated(IUser user) {
//            Log.e(LOG_TAG, "onUserTalkStateUpdated");
/*
            Log.e("Mumble: onUserTalkStateUpdated: user = "
                    + user.getName() + " | Session: " + user.getSession() + " | Channel: " + user.getChannel()
                    + " | Muted: " + user.isMuted() + " | Deafend: " + user.isDeafened()
                    + " | SelfMuted: " + user.isSelfMuted() + " | SelfDeafend: " + user.isSelfDeafened()
                    + " | LocalMuted: " + user.isLocalMuted()
                    + " \n " + user.toString() );*/
        }

        @Override
        public void onNotSyncronizedException(String ex) {
            Log.e(LOG_TAG, "onNotSyncronizedException");
        }

        @Override
        public void onUserConnected(IUser user) {
            Log.e(LOG_TAG, "onUserConnected: " + (user != null ?
                    (user.getUserId() + "." + user.getName()) : ""));
            if(connectionListener != null) {
                int assetId = 0;
                try
                {
                    assetId = Integer.parseInt(user.getName());
                }
                catch (Exception ex) {}
                if(assetId > 0)
                    connectionListener.onAssetConnected(assetId);
            }
        }

        @Override
        public void onUserRemoved(IUser user, String reason) {
            Log.e(LOG_TAG, "onUserRemoved: " + (user != null ?
                    (user.getUserId() + "." + user.getName()) : ""));

            if(connectionListener != null) {
                int assetId = 0;
                try
                {
                    assetId = Integer.parseInt(user.getName());
                }
                catch (Exception ex) {}
                if(assetId > 0)
                    connectionListener.onAssetDisconnected(assetId);
            }
        }

    };


    /**
     * Force the Mumble to remove the certificate in order to generate a new one
     * that will be used at the next login
     */
    public void removeCertificate() {
        // do this only if the database and settings were created
        if (mSettings != null && getDatabase() != null) {
            // remove the certificate
            getDatabase().removeCertificate(mSettings.getDefaultCertificate());
            // flag that no certificate is available
            mSettings.setDefaultCertificateId(-1);
        }
    }

    //interface AudioRegisterResponseListener

    public MumbleModule(Context context) {
        this.context = context;
        mDatabase = new PlumbleSQLiteDatabase(context);
        mSettings = Settings.getInstance(context);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.registerOnSharedPreferenceChangeListener(this);

    }

    public long lastInitializeTime = (new Date()).getTime() - 7000;
    public void initializeMumble(String username, String password) {
        Log.e(LOG_TAG, "+++ InitializeMumble "
                + ("isUsingCertificate: " + mSettings.isUsingCertificate()));

        long crtTimeMs = (new Date()).getTime();

        if(crtTimeMs - lastInitializeTime < 5 * 1000)
        {
            Log.e(LOG_TAG, "Trying to initializeMumble faster than 5 seconds, cancel");
            return;
        }

        this.username = username;
        this.password = password;

        //if(regStatus != RegistrationStatus.REGISTRATION_UNSUCCESSFUL)
        setRegStatus(RegistrationStatus.REGISTRATION_UNSUCCESSFUL);

        mDatabase.open();

        lastInitializeTime = (new Date()).getTime();

        // generate a certificate if it doesn't exist
        if (!mSettings.isUsingCertificate()) {
            /*
            int port = 64738;
            try {
                port = Integer.parseInt(serverPort);
            }
            catch (Exception ex) {;}


            //final Server server = new Server(1, "safemobile", "18.195.50.16", 64738, "default", "");
            final Server server = new Server(1, "safemobile", serverIP, port, username, password);

            List<Server> servers = mDatabase.getServers();
            for(Server s : servers)
                mDatabase.removeServer(s);

            mDatabase.addServer(server);
            */
            //pdateServerInfo();

            Log.e(LOG_TAG,  "Mumble needs to generate a certificate");
            PlumbleCertificateGenerateTask generateTask = new PlumbleCertificateGenerateTask(context) {
                @Override
                protected void onPostExecute(DatabaseCertificate result) {
                    super.onPostExecute(result);
                    if (result != null) {
                        Log.e(LOG_TAG, "MumbleModule PlumbleCertificateGenerateTask " + result.getId());
                        mSettings.setDefaultCertificateId(result.getId());

                        if (connectionListener != null)
                            connectionListener.onCertificateGenerated(result.getId());

                        startMumbleService(getMumbleServerAtIndex(0));
                    }
                    else
                    {
                        Log.e(LOG_TAG, "Unable to generate the certificate on mumble ");
                    }
                }
            };
/*
            String alias = server.getHost();
            KeyStore trustStore = PlumbleTrustStore.getTrustStore(context);
            trustStore.setCertificateEntry(alias, x509);
            PlumbleTrustStore.saveTrustStore(context, trustStore);
            Toast.makeText(context, R.string.trust_added, Toast.LENGTH_LONG).show();
*/

            generateTask.execute();
        } else {
            startMumbleService(getMumbleServerAtIndex(0));
        }
    }

    public boolean isInitialized() {
        return mSettings != null && mSettings.isUsingCertificate()
                && mService != null;
    }

    public void closeMumble() {
        Log.e(LOG_TAG, "Closing Mumble...");

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.unregisterOnSharedPreferenceChangeListener(this);
        mDatabase.close();

        setRegStatus(RegistrationStatus.REGISTRATION_UNSUCCESSFUL);
    }

    private ServerConnectTask connectTask;


    public Date lastMumbleStart = new Date(1970,0,0);
    public final int MIN_START_INTERVALS_MS = 5000;

    public void startMumbleService(final Server server) {
        if(server == null)
        {
            Log.e(LOG_TAG, "No server configuration provided");
            if(connectionListener != null)
                connectionListener.onNoServerConfigurationProvided();
            return;
        }

        Log.e(LOG_TAG,"startMumbleService : " + server.toString());
/*
        if(new Date().getTime() - lastMumbleStart.getTime() < MIN_START_INTERVALS_MS)
        {
            Log.e(LOG_TAG, "startMumbleService called faster that " + MIN_START_INTERVALS_MS + "ms");
            return;
        }

        lastMumbleStart = new Date();
*/
        if(mMumbleLock == null) {
            PowerManager pm = (PowerManager) context.getSystemService(POWER_SERVICE);
            mMumbleLock = pm.newWakeLock(MUMBLE_WAKE_LOCK, LOG_TAG);
        }

        if(mMumbleLock != null)
            mMumbleLock.acquire(500);


        Log.e(LOG_TAG, "+++ startMumbleService: " + server.toString());
        // Check if we're already connected to a server; if so, inform user.
        if (mService != null && mService.isConnected()) {/*
            // Register an observer to reconnect to the new server once disconnected.
            mService.registerObserver(new JumbleObserver() {
                @Override
                public void onDisconnected(JumbleException e) {
                    startMumbleService(server);
                    mService.unregisterObserver(this);
                }
            });*/
            mService.disconnect();
            return;
        }

        Intent connectIntent = new Intent(context.getApplicationContext(), PlumbleService.class);
        context.getApplicationContext().bindService(connectIntent, mMumbleConnection, 0);

        try {
            connectTask = new ServerConnectTask(context, mDatabase);
            connectTask.execute(server);
        } catch (Exception ex) {
            startMumbleService(getMumbleServerAtIndex(0));
        }
    }

    public void stopMumbleService() {

        Log.e(LOG_TAG,"stopMumbleService");

        if(mMumbleLock != null && mMumbleLock.isHeld())
            mMumbleLock.release();

        mMumbleLock = null;

        closeMumble();

        if (connectTask != null)
            connectTask.cancel(true);

        if (mService != null) {
            Log.e(LOG_TAG, "Stopping Jumble Service");
            mService.cancelReconnect();
            try {
                mService.unregisterObserver(mObserver);
            }
            catch (Exception ex)
            {
                Log.e(LOG_TAG, "Error unregistering observer for service : " + ex.toString());
            }

            mService.disconnect();
            mService.stopSelf();
            mService = null;
        }

        if (debugTimer != null)
            debugTimer.cancel();

        debugTimer = null;

        try {
            context.getApplicationContext().unbindService(mMumbleConnection);
        } catch (Exception ex) {
            Log.e(LOG_TAG, "Mumble service is not bound to unbind it");
        }

    }

    /**
     * Force the SIP to send again the register message in order to update it's IP address
     */
    public void reRegister() {
        stopMumbleService();

        if (!isInitialized())
            initializeMumble(username, password);
        else
            startMumbleService(getMumbleServerAtIndex(0));
    }


    public boolean getForceTCP() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(Settings.PREF_FORCE_TCP, false);
    }


    public void setForceTCP(boolean isForceTCP) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        preferences.edit().putBoolean(Settings.PREF_FORCE_TCP, isForceTCP).apply();

    }


    /**
     * Flag in the mumble server that a whisper needs to be done with the provided asset id.
     * The asset id corresponds to the mumble user name
     * @param assetId The asset id that needs to be called in a whisper
     * @param assetSipId The sip identifier in the voice world
     */
    public void preparePrivateCall(long assetId, long assetSipId)
    {
        Log.e(LOG_TAG, "preparePrivateCall " + assetId + " | " + assetSipId);
        //call(assetSipId);

        // Unregister any existing voice target.
        leavePrivateCall(assetId, assetSipId);

        if(mService != null) {
            boolean res = mService.setPrivateCallAsset(assetId);

            Log.e(LOG_TAG, "preparePrivateCall result is : " + res);
        }
    }

    /**
     * Flag in the mumble server that a whisper isn't need anymore with the provided asset id.
     * The asset id corresponds to the mumble user name
     * @param assetId The asset id that doesn't need to be called
     * @param assetSipId The sip identifier in the voice world
     */
    public void leavePrivateCall(long assetId, long assetSipId)
    {
        try
        {
            if(mService != null) {
                byte whisperTargetId = mService.whisperId;

                Log.e(LOG_TAG, "leavePrivateCall " + assetId + " | " + assetSipId
                        + " | " + whisperTargetId);
                mService.unregisterWhisperTarget(whisperTargetId);
                mService.setVoiceTargetId((byte) 0);
            }
        }
        catch (IllegalArgumentException ex)
        {

        }
    }



    /**
     * Get the Service stored at a specific index in the server List.
     * The server list is stored in the Mumble Database and can have multiple
     * server stored in
     *
     * @param i The index of the server in the server list
     * @return A server instance from the Mumble Database
     */
    public Server getMumbleServerAtIndex(int i) {
        try {
            List<Server> serverList = mDatabase.getServers();

            if ((serverList != null) && (serverList.size() > i))
                return serverList.get(i);
        }
        catch (Exception ex)
        {
            Log.e(LOG_TAG, "getMumbleServerAtIndex " + i + " Exception: " +ex.toString());
            reRegister();
        }

        return null;
    }

    public void removeMumbleServerAtIndex(int i) {
        Server s = getMumbleServerAtIndex(i);
        if (s != null)
            mDatabase.removeServer(s);
    }

    @Override
    public PlumbleDatabase getDatabase() {
        return mDatabase;
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //Log.e(LOG_TAG, "Mumble on settings " + key + " changed");
    }


    @Override
    public IPlumbleService getService() {
        return mService;
    }

    @Override
    public void addServiceFragment(JumbleServiceFragment fragment) {
//        mServiceFragments.add(fragment);
    }

    @Override
    public void removeServiceFragment(JumbleServiceFragment fragment) {
//        mServiceFragments.remove(fragment);
    }


    private String serverIP = "";
    private String serverPort = "";

    public void SetServerIPandPort(String serverIP, String serverPort) {
        this.serverIP = serverIP;
        this.serverPort = serverPort;
    }


    public void updateServerInfo(String username, String password) {
        int port = 64738;
        try {
            port = Integer.parseInt(serverPort);
        } catch (Exception ex) {
            ;
        }


        //final Server server = new Server(1, "safemobile", "18.195.50.16", 64738, "default", "");
        final Server server = new Server(1, "safemobile", serverIP, port, username, password);

        List<Server> servers = mDatabase.getServers();
        for (Server s : servers)
            mDatabase.removeServer(s);

        mDatabase.addServer(server);
    }


    //region PUBLIC METHODS



    /**
     * Return the current registration status of the Voice Module
     *
     * @return an enum value for the current registration status
     */
    public RegistrationStatus getRegistrationStatus() {
        return regStatus;
    }

    //endregion

    private boolean isInCall = false;
    private boolean isIncommingCall = false;
    private Date callTime = new Date(1970,0,0);
    private CryptState callStartState = null;


    //region IVOIP Interface methods


    private boolean currentMuted = false;
    private boolean currentDeafen = false;

    public void updateMicAndSpeakerForCall(boolean isMicOn, boolean isSpeakerOn)
    {
        Log.e(LOG_TAG, "updateMicAndSpeakerForCall: Mic:" + (isMicOn) + " | speaker: " + isSpeakerOn);
        // get if isEmergency by the logged user to leave the mic on
        boolean isEmergency = false;
        isMicOn = (isMicOn || isEmergency);

        isSpeakerOn = isSpeakerOn ;

        /*eionut: play with mute and deaf*/
        if (mService != null && mService.isConnected()) {
            //IUser self = mService.getSession().getSessionUser();
            boolean muted = !isMicOn;
            boolean deafened = !isSpeakerOn;

            if(currentDeafen == muted && currentDeafen == deafened) {
                Log.e(LOG_TAG, "Trying to set mumble mic and speaker twice");
                //return;
            }
            else
            {
                currentDeafen = deafened;
                currentMuted = muted;
            }

            deafened &= muted; // Undeafen if mute is off

            Log.e(LOG_TAG, "setSelfMuteDeafState");
            mService.getSession().setSelfMuteDeafState(muted, deafened);

            //if (!isSpeakerOn)
            {
                if (isMicOn) {
                    Log.e(LOG_TAG, "onTalkKeyDown");
                    mService.onTalkKeyDown();
                }
                else {
                    Log.e(LOG_TAG, "onTalkKeyUp");
                    mService.onTalkKeyUp();
                }
            }
            Log.e(LOG_TAG, "setTalkingState");
            mService.getSession().setTalkingState(isMicOn);
            Log.e(LOG_TAG, "done setting MicAndSpeaker");
        }
    }

    //endregion

    private IChannel findChannelWithName(String name, IChannel parentChannel) {
        if (parentChannel.getName().contentEquals(name))
            return parentChannel;

        for (IChannel subc : (List<IChannel>) parentChannel.getSubchannels()) {
            //Log.e("channel: id = " + subc.getId() + " name = " + subc.getName());
            IChannel ic = findChannelWithName(name, subc);
            if (ic != null)
                return ic;
        }

        return null;
    }


    public void setSpeakers(boolean speakersOn) {
        Bundle changedExtras = new Bundle();
        changedExtras.putInt(JumbleService.EXTRAS_AUDIO_STREAM,
                speakersOn ? AudioManager.STREAM_MUSIC : AudioManager.STREAM_VOICE_CALL);

        if (mService != null) {
            try {
                // Reconfigure the service appropriately.
                mService.configureExtras(changedExtras);
            } catch (AudioException e) {
                e.printStackTrace();
            }
        }
    }


}
