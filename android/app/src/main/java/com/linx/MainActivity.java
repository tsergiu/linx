package com.linx;

import android.os.Handler;
import android.widget.Toast;

import com.facebook.react.ReactActivity;
import com.safemobile.linx.Enums.RegistrationStatus;
import com.safemobile.linx.Modules.MumbleModule;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "linx";
  }


  private MumbleModule mumbleModule;
  private Handler handler = new Handler();


  @Override
  public void onStart()
  {
    super.onStart();

    if(mumbleModule == null) {
      String voiceAddress = "stage.linx.safemobile.com";
      String voicePort = "5668";
      mumbleModule = new MumbleModule(this);
      mumbleModule.SetServerIPandPort(voiceAddress, voicePort);
      mumbleModule.setForceTCP(false);

      mumbleModule.setConnectionListener(new MumbleModule.MumbleListener() {
        @Override
        public void onConnecting() {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onConnected", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onConnected() {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onConnected", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onDisconnected() {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onDisconnected", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onConnectionDropped(String reason) {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onConnectionDropped", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onWrongCertificate() {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onWrongCertificate", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onCertificateGenerated(long certId) {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onCertificateGenerated", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onRegistrationStatusChanged(RegistrationStatus status) {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onRegistrationStatusChanged: " + status.toString(), Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onNoServerConfigurationProvided() {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onNoServerConfigurationProvided", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onAssetConnected(long assetId) {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onAssetConnected", Toast.LENGTH_SHORT).show());
        }

        @Override
        public void onAssetDisconnected(long assetId) {
          handler.post(() -> Toast.makeText(MainActivity.this,
                  "onAssetDisconnected", Toast.LENGTH_SHORT).show());
        }
      });

      mumbleModule.updateServerInfo("React-Android", "safemobile123");
      Thread t = new Thread(() -> {
        if (mumbleModule != null)
          mumbleModule.initializeMumble("React-Android", "safemobile123");
      });

      t.start();
    }
  }

  @Override
  public void onStop() {
    super.onStop();

    if(mumbleModule != null) {
      mumbleModule.stopMumbleService();
      mumbleModule = null;
    }
  }

}
