//  Created by react-native-create-bridge

import { NativeModules } from 'react-native'

const { HelloNativeModule } = NativeModules

export default {
  exampleMethod () {
    return HelloNativeModule.exampleMethod()
  },

  EXAMPLE_CONSTANT: HelloNativeModule.EXAMPLE_CONSTANT
}
