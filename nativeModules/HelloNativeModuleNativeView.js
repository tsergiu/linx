//  Created by react-native-create-bridge

import React, { Component } from 'react'
import { requireNativeComponent } from 'react-native'

const HelloNativeModule = requireNativeComponent('HelloNativeModule', HelloNativeModuleView)

export default class HelloNativeModuleView extends Component {
  render () {
    return <HelloNativeModule {...this.props} />
  }
}

HelloNativeModuleView.propTypes = {
  exampleProp: React.PropTypes.any
}
