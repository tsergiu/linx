/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, Fragment } from 'react';

import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  NativeModules,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

console.log('NativeModules', NativeModules)
const myModule = NativeModules.HelloNativeModule

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      test: ['INITIAL VALUE']
    };
  }

  async componentDidMount() {
    try {
      let response = await fetch(
        'https://stage.linx.safemobile.com/api/generate-token',
      );
      let responseJson = await response.json();
      console.log('token', responseJson.data.token)
      this.setState({ token: responseJson.data.token });
    } catch (error) {
      console.error(error);
    }
  }

  render() {

    const { test } = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <View>
          <Text>
            {this.state.token}
          </Text>
        </View>
        <View>
          <Button title = "Test Native Bridge!" onPress = {()=>{
            myModule.MyBridgeMethod("Its from JavaScriptCode", (fromJavaCode) => {
              console.log(fromJavaCode);
              this.setState({ test: test.concat('BUTTON PRESSED') });
            })
          }}>
          </Button>
        </View>
        {
          test.map((e, i)=><Text key={i}>{e}</Text>)
        }
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});